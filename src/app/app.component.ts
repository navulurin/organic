import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { SignupPage } from '../pages/signup/signup';
import { SigninPage } from '../pages/signin/signin';

import { Utils } from '../services/utils';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  util:Utils;
  rootPage:any = SignupPage;

  openPage(p) {
    this.util.page=p;
    this.navigateToPage();
  }

  navigateToPage = () => {
      switch(this.util.page){
      case 'signup':
        this.rootPage = SignupPage;
        break;
      case 'signin':        
        this.rootPage = SigninPage;
        break;
    }
  }

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen , util : Utils) {
    this.util=util;
    this.util.pageUpdated.subscribe(
      (page) => {
        this.navigateToPage();
      }
    );

    platform.ready().then(() => { 
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
