import {Injectable,EventEmitter} from '@angular/core';

@Injectable()
export class Utils{
  	pageUpdated:EventEmitter<string> = new EventEmitter<string>();

	public page:string = 'signin';
	constructor(){

	}	
	setPage(page){
		this.page=page;
		this.pageUpdated.emit(this.page);
	}

	getPage = () => this.page;	
}