import {Component} from '@angular/core';
import { Utils } from '../../services/utils';

@Component({
  selector:'sign-in',
  templateUrl:'signin.html'
})

export class SigninPage{
  password:string= '';
  mobile:number;
  visiblePassword:boolean =false; 

  constructor(public  util : Utils) {

  }

  showPassword = () => {
    this.visiblePassword = true;
  }

  hidePassword = () =>{
    this.visiblePassword = false;
  }

   openPage(p) {
    this.util.setPage(p);
  }

}