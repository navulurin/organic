import { Component } from '@angular/core';
import { GooglePlus,Facebook, NativeStorage } from 'ionic-native';
import { Platform } from 'ionic-angular';
import { Utils } from '../../services/utils';

@Component({
  selector: 'signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
	password:string= '';
	mobile:number;
	visiblePassword:boolean =false;	
	name:string;
	mail:string;	

  constructor(platform: Platform ,public  util : Utils) {

  }

  showPassword = () => {
	  this.visiblePassword = true;
  }

  hidePassword = () =>{
	  this.visiblePassword = false;
  }

  googleSignIn = function() {
        this.handleCordovalogin();
  }

  handleCordovalogin = function(){
       GooglePlus.login({})
      .then(
      (res) => {
        alert(res);
      },
      (err) => {
        console.log('error');
        console.log(err);
      });
 
  }

  openPage(p) {
    this.util.setPage(p);
  }

  doFbLogin = () => {
    let permissions = new Array<string>();
    //the permissions your facebook app needs from the user
    permissions = ["public_profile"];


    Facebook.login(permissions)
    .then(function(response){
      let userId = response.authResponse.userID;
      let params = new Array<string>();

      //Getting name and gender properties
      Facebook.api("/me?fields=name,gender", params)
      .then(function(user) {
        user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
        //now we have the users info, let's save it in the NativeStorage
        NativeStorage.setItem('user',
        {
          name: user.name,
          gender: user.gender,
          picture: user.picture
        });
        alert(user);
      })
    }, function(error){
      console.log(error);
    });
  }

  doFbLogout = () => {
    Facebook.logout()
    .then(function(response) {
      //user logged out so we will remove
    }, function(error){
      console.log(error);
    });
  }


}
